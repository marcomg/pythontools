#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import argparse;
import random;
import string;

def rand(size, chars):
    y = '';
    for x in range(size):
        y += random.choice(chars);
    return y;

argparse = argparse.ArgumentParser(prog='genpsswd', description='A program to generate a pseudo-casual password');
argparse.add_argument('--version', action='version', version='%(prog)s version 1.0');

argparse.add_argument('size', action='store', type=int, help='how many chars have to be the password');
argparse.add_argument('-l', '--lower', action='store_true', help='add lowercase letters');
argparse.add_argument('-u', '--upper', action='store_true', help='add uppercase letters');
argparse.add_argument('-d', '--digits', action='store_true', help='add numers (form 0 to 9)');
argparse.add_argument('-s', '--special', action='store_true', help='add special letters');
argparse.add_argument('-p', '--personal', action='store', help='add a personal string to generate a password');
args = argparse.parse_args();

chars = '';

if args.lower:
    chars += string.ascii_lowercase;
if args.upper:
    chars += string.ascii_uppercase;
if args.digits:
    chars += string.digits;
if args.special:
    chars += '''`~!@#$%^&*()_-+={}[]\|:;"'<>,.?/;''';
if args.personal:
    chars += args.personal;

# if nothing selected
if not (args.lower or args.upper or args.digits or args.special or args.personal):
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits;

print(rand(args.size, chars));