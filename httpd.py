#!/usr/bin/python

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import argparse
import BaseHTTPServer
import SimpleHTTPServer
import time
import threading

# sleep forever
def waitForever():
  while True:
    time.sleep(60)

# set arguments
argParse = argparse.ArgumentParser(description='A simple HTTP server', prog='Simple HTTP sever')
argParse.add_argument('-v', '--version', action='version', version='%(prog)s version 1.0')
argParse.add_argument('-p', '--port', action='store', default=8080, type=int, help='the port on run the HTTP server')
argParse.add_argument('-t', '--threads', action='store', default=10, type=int, help='the number of threads of the HTTP server (the max number of users to serve contemporaney)')
args = argParse.parse_args()

# copy classes
handler = SimpleHTTPServer.SimpleHTTPRequestHandler
server = BaseHTTPServer.HTTPServer

# Load the server
try:
  server_address = ('', args.port)
  httpd = server(server_address, handler)
except BaseHTTPServer.socket.error as exceptError:
  exceptError = str(exceptError)
  if exceptError == '[Errno 13] Permission denied':
    print '[ERROR]: permission deined to use the port %d' % (args.port)
  elif exceptError == '[Errno 98] Address already in use':
    print '[ERROR]: the port %d is already in use. Please change it!' % (args.port)
  else:
    print '[ERROR]: Unknow error, the except say: %s' % (exceptError)
  exit()

# say message
print "Started HTTP server on port %s" % (server_address[1])

# load threads to work
i = 0
numberThreads = args.threads # number of contemporaney requests

threads = []
while i < numberThreads:
  threads.append(threading.Thread(target=httpd.serve_forever))
  threads[i].setDaemon(1)
  threads[i].start()
  i += 1
print 'Loaded other %d thread%s' % (numberThreads, 's'[numberThreads == 1:])

# wait to stop
try:
  waitForever()
except KeyboardInterrupt:
  print ' is caught, stopped HTTP server an his %d thread on port %s' % (numberThreads, server_address[1])
