#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import os;
import glob;

print('###################################');
print('# BENVENUTI IN MENCODER INTERFACE #');
print('# UNA INREFACCIA PER SEMPLIFICARE #');
print('# MOLTO  MENCODER E LAVORARE  UNA #');
print('# CARTELLA. LA LICENZA È LA GPLv3 #');
print('###################################');

patch = '';

# Take dir
while(patch == ''):
    patch = input("Si prega di posizionarsi nella cartella da convertire:\n");

# Check input (it have to have / at the begin an the end)
if(patch[-1] == '/'):
    pass;
else:
    patch = patch + '/';

if(patch[0] == '/'):
    pass;
else:
    patch = '/' + patch;

# Get a list with files
os.chdir(patch);
files = glob.glob(patch + '*.mkv');

# Work
for thisFile in files:
    firstPass = "mencoder -o /dev/null -oac mp3lame -ovc xvid -xvidencopts pass=1 '" + thisFile + "'";
    secondPass = "mencoder -o '" + thisFile + "'.avi -oac mp3lame -ovc xvid -xvidencopts pass=2:bitrate=2000 '" + thisFile + "'";

    os.system(firstPass);
    os.system(secondPass);

    print('###################################');
    print('###################################');
    print('###################################');

    os.system('rm divx2pass.log');

# Print finish message
print('###################################');
print('###################################');
print('###################################');
print('FINITO!');