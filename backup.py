#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import os;
import os.path;
import shutil;

# FUNZIONI
# Controllo che l'input abbia una / sia all'inizio che alla fine, altrimenti lo aggiungo
def perfectPatch(patch):
    if(patch[-1] == '/'):
        pass;
    else:
        patch = patch + '/';

    if(patch[0] == '/'):
        pass;
    else:
        patch = '/' + patch;
    return patch;

# Prendo la lista di file da una cartella ricorsivamente
def getRecoursiveFileList(root):
    fileList = [];
    for root, subFolders, files in os.walk(root):
        for file in files:
            fileList.append(os.path.join(root,file));
    return fileList;

# Prendo una lista con la dimensione dei file di una lista
def getFileSizeList(lfiles):
    size = [];
    for vfile in lfiles:
        size.append(os.path.getsize(vfile));
    return size;

# Prendo un input a patto che non sia nullo, altrimenti ripeto
def getInput(message):
    vinput = '';
    while(vinput == ''):
        vinput = input(message);
    return vinput.strip();

# Conversione da un valore umano a bytes
def getByte(value):
    last = value[-1];
    if(value.isdigit()):
        pass;
    else:
        value = value[:-1];
    
    value = float(value);
    if(last == ('k' or 'K')):
        return value * 1024;
    elif(last == ('m' or 'M')):
        return value * 1048576;
    elif(last == ('g' or 'G')):
        return value * 1073741824;
    elif(last == ('t' or 'T')):
        return value * 1099511627776;
    else:
        return int(value);

# Conversione da bytes ad un valore umano
def getHumanValue(val):
    if(val >= 1024):
        val = val/1024;
        if(val < 1024):
            return(str((val))+'KiB');
    if(val >= 1024):
        val = val/1024;
        if(val < 1024):
            return(str(int(val))+'MiB');
    if(val >= 1024):
        val = val/1024;
        if(val < 1024):
            return(str(int(val))+'GiB');
    if(val >= 1024):
        val = val/1024;
        if(val < 1024):
            return(str(int(val))+'TiB');
    else:
        return(str(int(val))+'B');

# Crea tutte le cartelle in un percorso se non esistono
def makedirs(patch):
    patch = os.path.realpath(patch);
    if os.path.exists(patch):
        pass;
    else:
        os.makedirs(patch);

def patchDiff(original, partial):
    leng = len(partial);
    leng -= 1;
    output = original[leng:];
    return output;

def superCopyFile(source, output):
    makedirs(source);
    shutil.copy(source, output);

# SCRIPT

# Prendo la direcotry da backuppare
patch = getInput("Inserire il percorso della cartella da backuppare:\n");
patch = perfectPatch(patch);

# Lavoro prendendo la lista e la dimensione dei files
os.chdir(patch);
files = getRecoursiveFileList(patch);
sizes = getFileSizeList(files);

# Prendo la direcotry di lavoro
output = getInput("Inserire il percorso della cartella di lavoro:\n");
output = perfectPatch(output);
os.chdir(output);

# Prendo la dimensione del supporto
supportSize = getInput("Inserire la dimensione del supporto di input, puoi far seguire (senza spazio) K, M o G per indicare Kilobibytes, Megabibytes o Gigabibytes:\n");
supportSize = getByte(supportSize);

# Stampo un messaggio di avvertimento della lentezza della procedura
print("Il sistema ora inizierà a copiare i file dalla cartella di origine ad alcune cartelle che si trovano in quella di output. L'operazione potrà essere molto lenta a seconda della dimensione dei dati, non verrà stampato niente a video fino ad operazione conclusa, quindi abbiate pazienza, se per caso avete paura che l'applicazione si sia inchiodata guardate se l'HDD macina ;)");

# Raggruppo i files in liste
i = 0;
while(files != []):
    i += 1;
    tmpRSize = 0; # Dimensione del gruppo temporaneo
    tmpList = []; # Lista temporanea
    secondWhile = 1; # Valore dei tentativi di partenza
    whileSize = 0; # Dimensione dei dati durante il while (se il for non aggiunge niente viene interrotto)
    # Scorro la lista
    while(secondWhile <= 2): # Se il for qui sotto non fa nulla cambio gruppo
        # Controllo che il for lavori, se non lavora aumento i tentetivi per far interrompere il ciclo
        if(whileSize == tmpRSize):
            secondWhile += 1;
        else:
            whileSize = tmpRSize;
        
        # Scorro i file da aggiungere
        for tmpFile in files:
            idx = files.index(tmpFile);
            tmpSize = sizes[idx];
            # se i files entrano nel disco li aggiungo, altrimenti finisco il disco
            if((tmpRSize + tmpSize) <= supportSize):
                tmpRSize += tmpSize;# Aggiorno la dimensione
                tmpList.append(files[idx]);# aggiungo il percorso ad una lista temporanea
                
                # rimuovo gli item dalla lista
                del files[idx];
                del sizes[idx];
    
    # se la lista temporanea è vuota non riesco ad inserire i files nel range
    if(tmpList == []):
        print('Errore, alcuni file sono più grandi del volume quindi non riesco a sistemarli. I files sono:');
        print(files);
        exit();
    # se non è vuota posso procedere a salvare i dati
    else:
        os.mkdir(str(i));
        for tfile in tmpList:
            superCopyFile(tfile, './' + str(i) + patchDiff(tfile, patch));
print("Ho finito di macinare l'HDD!!\nCiao ciao!");