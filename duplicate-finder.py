#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import argparse;
import os;
import os.path;
import hashlib;

# FUNZIONI
# Controllo che l'input abbia una / alla fine, altrimenti la aggiungo
def perfectPatch(patch):
    if(patch[-1] == '/'):
        pass;
    else:
        patch = patch + '/';
    return patch;

# Prendo la lista di file da una cartella ricorsivamente
def getRecoursiveFileList(root):
    fileList = [];
    for root, subFolders, files in os.walk(root):
        for file in files:
            fileList.append(os.path.join(root,file));
    return fileList;

def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5();
        while True:
            data = fh.read(8192);
            if not data:
                break;
            m.update(data);
        return m.hexdigest();

def md5Sums(files):
    sums = [];
    for myfile in files:
        sums.append(md5Checksum(myfile));
    return sums;

argparse = argparse.ArgumentParser(prog='duplicate-finder', description='A program to find files duplicates in a directory');
argparse.add_argument('--version', action='version', version='%(prog)s version 1.0');

argparse.add_argument('patch', action='store', type=str, help='patch where search for duplicates');
argparse.add_argument('-v', '--verbose', action='store_true', help='enable verbose mode');

args = argparse.parse_args();

# Prendo il percorso da elaborare
patch = args.patch;

# Se quello che inserisco non è una cartella stampo une errore
if not os.path.isdir(patch):
    print('Error: you have to insert a directory patch');
    exit();

# Prendo tutti i file
if(args.verbose == True):
    print('Getting file list');
files = getRecoursiveFileList(patch);

# Prendo le somme md5
if(args.verbose == True):
    print('Making md5sums');
sums = md5Sums(files);

# Divido i dati in un dizionario {sum : [percorsi, vari]}
if(args.verbose == True):
    print('Processing data');
associatedSums = {};
i = 0;
while (i+1) <= len(sums):
    tmpSum = sums[i];
    tmpFile = files[i];
    
    # Se non esiste creo
    if not tmpSum in associatedSums:
        associatedSums[tmpSum] = [tmpFile];
    # altrimenti aggiungo
    else:
        associatedSums[tmpSum].append(tmpFile);
    i+=1;

# Salvo solo i file duplicati
duplicates = {};
for tmpSum in associatedSums:
    if len(associatedSums[tmpSum]) > 1:
        duplicates[tmpSum] = associatedSums[tmpSum];

# Preparo per la stampa
if duplicates == {}:
    print('No duplicates found');
else:
    print('Found %d duplicate%s' % (len(duplicates), 's'[len(duplicates) == 1:]));
    for key in duplicates:
        print('MD5: ' + key);
        data = duplicates[key];
        for d in data:
            print(d);
