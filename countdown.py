#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import argparse;
from sys import stdout;
from os import system;
import time;

def sto(tx, nl=False):
    if nl == False:
        stdout.write("\r" + str(tx));
    else:
        stdout.write("\r" + str(tx) + "\n");

def bell():
    system("play -q '/home/marco/Musica/Santana/Ultimate Santana/14 Santana - Europa (Earth'\\''s Cry Heaven'\\''s Smile).flac'");
    pass;

try:
    argparse = argparse.ArgumentParser(prog='countdown', description='A program for countdown');

    argparse.add_argument('--version', action='version', version='%(prog)s version 1.0');
    argparse.add_argument('-s', '--seconds', action='store', type=int, help='add seconds');
    argparse.add_argument('-m', '--minutes', action='store', type=int, help='add minutes');
    argparse.add_argument('-o', '--hours', action='store', type=int, help='add hours');
    argparse.add_argument('-d', '--days', action='store', type=int, help='add days');

    args = argparse.parse_args();

    timer = 0;

    if args.seconds is not None:
        timer += args.seconds;
    if args.minutes is not None:
        timer += args.minutes * 60;
    if args.hours is not None:
        timer += args.hours * 3600;
    if args.days is not None:
        timer += args.days * 86400;
    if timer == 0:
        argparse.print_help();
        exit();

    startime = time.time();

    while True:
        nowtime = time.time();
        if (nowtime - startime) <= timer:
            sto('Left time: ' + time.strftime('%X', time.gmtime(timer - (nowtime - startime))));
            time.sleep(1);
        else:
            sto('', True);
            print('Finito, avvio il suono');
            bell();
            print('Exiting');
            break;
except KeyboardInterrupt:
    print("\nKeyboardInterrupt");