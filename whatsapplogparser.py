#!/usr/bin/python3
import argparse

# Separator of the log
firstSplitter = ' - ';
secondSplitter = ': ';

# Arguments
argParse = argparse.ArgumentParser(description='Create a formatted (in HTML5) document from a log chat of whatsapp.', prog='WhatsApp chat parser.')
argParse.add_argument('patch', action='store', type=str, help='the patch of the log file')
args = argParse.parse_args()

# Get the file
try:
    chatLogF = open(args.patch, 'r')
    chatLog = chatLogF.read()
    chatLogF.close()
except FileNotFoundError:
    print('ERROR: Are you sure that the file exists?')

chatLogLs = chatLog.splitlines()
chatLogs = [ [], [], [] ]
for chatLogL in chatLogLs:
    try:
        chatLine = chatLogL.split(firstSplitter, 1)
        chatLine0 = chatLine[0]
        chatLine = chatLine[1].split(secondSplitter, 1)
        chatLine1 = chatLine[0]
        chatLine2 = chatLine[1]
        chatLogs[0].append(chatLine0)
        chatLogs[1].append(chatLine1)
        chatLogs[2].append(chatLine2)
    # A line not ok (last line?)
    except IndexError:
        pass

# chatLogs is a list: [data, person, text]
htmlBody = '<table>'

i = 0
while i < len(chatLogs[0]):
    htmlBody += '<tr><td>' + chatLogs[0][i] + '</td><td>' + chatLogs[1][i] + '</td><td>' + chatLogs[2][i] + '</td></tr>'
    i += 1
htmlBody += '</table>'

htmlFile = '''<!doctype html>
<html>
    <head>
        <title>Chat Log</title>
    </head>
    
    <body>
        ''' + htmlBody + '''
    </body>
</html>'''

print (htmlFile)