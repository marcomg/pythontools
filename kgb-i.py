#!/usr/bin/python3

########################################################################
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
#                                                                      #
# Copyright (C) 2013 by Marco Guerrini                                 #
########################################################################

import os;
import os.path;

#############
# FUNCTIONS #
#############
# Controllo che l'input abbia una / alla fine. Se non la ha l'aggiungo ritorno una stringa
def perfectPatch(patch):
    if(patch[-1] == '/'):
        pass;
    else:
        patch = patch + '/';

    if(patch[0] == '/'):
        pass;
    else:
        pass;
        #patch = '/' + patch;
    return patch;

# Prendo la lista di file da una cartella ricorsivamente, ritorno una lista con i percorsi
def getRecoursiveFileList(root):
    fileList = [];
    for root, subFolders, files in os.walk(root):
        for file in files:
            fileList.append(os.path.join(root,file));
    return fileList;

# Prendo un input non nullo, nel caso lo sia ricomincio.
def getInput(message):
    vinput = '';
    while(vinput == ''):
        vinput = input(message);
    return vinput.strip();

# Crea tutte le cartelle in un percorso se non esistono
def makedirs(patch):
    patch = os.path.realpath(patch);
    if os.path.exists(patch):
        pass;
    else:
        os.makedirs(patch);

# Crea cartelle per un file
def fileMakedirs(cfile):
    cfile = os.path.dirname(cfile);
    makedirs(cfile);

# Crea le cartelle nei percorsi un una lista
def makerdirs(l):
    for p in l:
        fileMakedirs(p);

# Prende la lista dei files da un archivio kgb
def getArchiveFileList(patch, toreturn = 'patch'):
    f = open(patch, 'rb');
    chars = f.read(15);# Prendo 15 caratteri dall'archivio giusto per cominciare, se ne prendo di più rischio di recuperare un carattere binario.
    kgb = chars[:10];# Ottengo i caratteri che indicano che l'archivio è KGB
    kgb = kgb.decode('utf-8');# lo codifico
    if(kgb == "KGB_arch -"):# Se è un archivio kgb contiente questo.
        pass;
    else:
        #Questo non è un archivio KGB!
        return(0);
    chars = chars[11:];# Prende i caretteri dopo l'header ovvero da \r\n, etc.
    # Ottengo la lista dei files divisa in: dimensione_in_byte [tab] percorso [newline]
    while 1:
        # Provo a decodificare i files fino a che non trovo un carattere binario che farà avvenire un'eccezione
        try:
            chars += f.read(1);# Prendo un altro file
            decoded = chars.decode('utf-8');# lo decodifico
        except:
            decoded = decoded[:-4];
            break;
    lines = decoded.splitlines();# divido l'output in linee
    del lines[0]; # cancello la prima che è vuota
    
    sizes = [];
    patches = [];
    
    for line in lines:
        tmp = line.partition('\t');
        sizes.append(tmp[0]);
        patches.append(tmp[2]);
    
    # In sizes c'è una lista con le dimensioni
    # in patches una con i percorsi
    if toreturn == 'patch':
        return(patches);
    else:
        return(sizes);
    

################
# SCRIPT START #
################
directory = getInput('Insert the patch of the direcotry to compress or the archive to extract: ');
directory = perfectPatch(directory);

###########
# EXTRAXT #
###########
if(os.path.isfile(directory[:-1])):
    files = getArchiveFileList(directory[:-1]);
    if not files:
        print('The file you inserted is not a KGB archive!');
        exit();
    makerdirs(files);
    command = 'kgb \'' + directory[:-1] + '\'';
    os.system(command);
    exit();
else:
    if not os.path.isdir(directory):
        print('Error: patch you inserted does not exists!!');
        exit();
output = getInput('Insert the name of the compressed archive: ');

############
# COMPRESS #
############
compressLivel = getInput('''
|----------      ----------------------------------|
| 0              2 MB (the fastest compression)    |
| 1              3 MB                              |
| 2              6 MB                              |
| 3              18 MB                             |
| 4              64 MB                             |
| 5              154 MB                            |
| 6              202 MB                            |
| 7              404 MB                            |
| 8              808 MB                            |
| 9              1616 MB (the best compression)    |
|----------      ----------------------------------|
''');

compressLivel = int(compressLivel);

# Control livel
if(compressLivel == 0):
    pass;
elif(compressLivel == 1):
    pass;
elif(compressLivel == 2):
    pass;
elif(compressLivel == 3):
    pass;
elif(compressLivel == 4):
    pass;
elif(compressLivel == 5):
    pass;
elif(compressLivel == 6):
    pass;
elif(compressLivel == 7):
    pass;
elif(compressLivel == 8):
    pass;
elif(compressLivel == 9):
    pass;
else:
    print('Error: insert a number between 0 and 9');
    exit();

try:
    files = getRecoursiveFileList(directory);
except:
    print('Error: patch does not exists');

# Creo i comandi per comprimere
stringFiles = '';
for sfile in files:
    stringFiles += ' ';
    stringFiles += "'" + sfile + "'";

command = 'kgb -' + str(compressLivel) + ' ' + output + '.kgb' + stringFiles;
os.system(command);